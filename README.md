#Descrição do trabalho 2 de REDES I
---
Montar uma rede em anel com 4 máquinas usando Socket DGRAM
+ O anel é unidirecional
+ Token Ring
+ Porta : (1024<) cada maquina deve usar uma porta diferentes

#UNO
##Regras:
 - Jogador com o bastao faz a jogada
    1)Depois de escolher a carta, envia mensagem infomando a carta q esta jogando
    e espera a mensagem voltar.
    2)Depois o jgoador passa o bastao para frente
    3)Antes de jogar a penultima carta (uno) deve mandar a mensagem de uno na rede
    esperar ela voltar para mandar a mensagem da carta jogada e só depois passar o bastao

Cartas:
  - 0-9 de cada cor (amarelo, azul, verde, vermelho)
  - 8 cartas de pular proximo jogador
  - 8 cartas de compra 2

Demais funcionalidades fica a cargo do grupo
