import random
from termcolor import colored
# import from __future__ import print_function


class Card:
    def __init__(self, number, color):
        self.number = number
        self.color = color


def createDeck():
    listNumbers = ["0" ,"1", "2", "3", "4", "5", "6", "7", "8", "9", "+2", "+2", "@", "@"]
    # listNumbers = ["@", "@", "@", "@", "@", "@", "@", "@", "@", "@", "@", "@", "@", "@"]
    listColors = ["red", "green", "yellow", "blue" ]
    deck = list()

    for i in listNumbers:
        for j in listColors:
            deck.append(Card(i,j))
    random.shuffle(deck)
    return deck


def drawCards(gameDeck, playerDeck, number, stack = None):
    over = False
    for i in range(number):
        if (len(gameDeck) == 0):
            random.shuffle(stack)
            gameDeck.extend(stack)
            stack = list()
            over = True
        card = gameDeck.pop()
        playerDeck.append(card)
    else:
        print
    return over 


def showCards(playerDeck):
    for i in playerDeck:
        print colored( ("({},{})".format(i.number, i.color)),i.color) ,
    print
