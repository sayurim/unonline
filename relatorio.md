#Relatório jogo em python2.7

Autores: Jean Carraro, Sayuri Morikane
Unonline.py cards.py

##Rede token ring
---
###Decisões sobre a rede
+ As configurções das conexoes sao feitas manualmente ao executar o arquivo : portas de recepção e envio, proxima maquina, se é mestre ou nao.
+ Devido chances de perda de mensagem inicial os escravos devem ser executados antes e estarem escutando quando o mestre foi iniciado.
+ O token é apenas uma mensagem estatica.
+ O número de jogadores pode ser facilmente aumentado com a mudança de algumas constantes NR_PLAYERS e a lista de cartas.

###Decisões sobre o jogo
+ O jogo começa com o jogador0 (com o token) embaralhando o deck de cartas e enviando para o proximo que retira suas cartas e envia o restande do deck para frente ate voltar para o jogador 0 que retira suas cartas e começa o jogo.
+ A carta inicial é comprada do baralho.
+ As cartas de cada jogador sao organizadas em listas e as escolhas do jogador deve ser o indice da carta na lista
+ Na sua vez o jogador pode escolher comprar ou jogar uma carta(se possível). O jogador SEMPRE pode escolher comprar.
+ Quando o jogador recebe a carta "+2" ele nao pode jogar uma carta na sua vez.
+ Quando o jogadore recebe a carta "@" ele nao pode jogar uma carta na sua vez.
+ No caso do deck de cartas de compra acabar, o jogador que precisa comprar pega sua stack(cartas que ja foram jogadas), embaralha, compra sua carta e envia o baralho para os proximos jogadores que só a armazenam.
+ Se nenhum jogador jogar cartas e comprarem todas as cartas do baralho vai dar erro mesmo.
