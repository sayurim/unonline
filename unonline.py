# -*- coding: UTF-8 -*-

import socket, threading
import time, signal
from cards import *
import random
import pickle

#const DEF
HOST = socket.gethostname()
RC_port = int(raw_input('Receber da Porta: '))
SD_port = int(raw_input('Enviar para a Porta: '))
TARGET = raw_input('Prox maquina: ')
has_token = bool(raw_input('mestre? '))
TOKEN = '1452'
N_PLAYERS = 4

#sockets
sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
receiver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
receiver.bind((HOST, RC_port))
sender.bind((HOST,SD_port))
deck = list()
stack = list()
myDeck = list()

#Deck initialization
if (has_token):
    deck = createDeck()
    data = pickle.dumps(deck)
    sender.sendto(data, (TARGET,SD_port))
    data, addr = receiver.recvfrom(3580) #deck size max  2397
    deck = pickle.loads(data)
    drawCards(deck, myDeck, 7)
else:
    data, addr = receiver.recvfrom(3580) #deck size max 2397
    deck = pickle.loads(data)
    drawCards(deck, myDeck, 7)
    data = pickle.dumps(deck)
    sender.sendto(data, (TARGET,SD_port))

#Fix players and Deck
players = {49:1, 42:2, 35:3, 28:0}
deck_size = len(deck)
id = players[deck_size]
while(len(deck) > 28):
    deck.pop()

player_turn = 0
in_game = True
blocked = False
lastCard = deck.pop()
stack.append(lastCard)
print 'Carta inicial'+ colored("({},{})".format(lastCard.number, lastCard.color), lastCard.color)

#game loop
while in_game:
    if (has_token):
        print 'Suas Cartas são'
        showCards(myDeck)

        if (not blocked):
            #move validation
            invalid_move = True
            while invalid_move:
                choice = raw_input('Escolha a posicao da carta(0:%s) ou B para comprar: '%(len(myDeck)-1))
                try:
                    index = int(choice)
                except ValueError:
                    index = -2

                if(index < len(myDeck) and index > -1):
                    if any([myDeck[index].color == lastCard.color,myDeck[index].number == lastCard.number]):
                        MESSAGE = pickle.dumps(myDeck[index])
                        invalid_move = False
                    else:
                        print 'Esta carta nao pode ser jogada'
                elif(choice == 'B'):
                    invalid_move = False
                else:
                    print 'Esta carta nao existe ou comando nao existe'

            #uno
            if (len(myDeck) == 2 and choice != 'B'):
                sender.sendto('UNO!',(TARGET,SD_port))
                print ('ESTOU DE UNO!!')
                data, addr = receiver.recvfrom(128)

            #draw card
            if (choice == 'B'):
                sender.sendto("compra1",(TARGET,SD_port))
                data, addr = receiver.recvfrom(64) #retira mensagem da rede
                deck_over = drawCards(deck, myDeck, 1, stack)
                if deck_over:
                    data = pickle.dumps(deck)
                    sender.sendto(data, (TARGET,SD_port))
                    data, addr = receiver.recvfrom(3580)   

                print "Comprei a carta "+ colored("({},{})".format(myDeck[len(myDeck)-1].number, myDeck[len(myDeck)-1].color),myDeck[len(myDeck)-1].color)
            else:
                sender.sendto(MESSAGE,(TARGET,SD_port))
                print "Joguei "+ colored("({},{})".format(myDeck[index].number, myDeck[index].color),myDeck[index].color)
                stack.append(myDeck[index])
                myDeck.pop(index)
                data, addr = receiver.recvfrom(128) #retira mensagem da rede

            #winner winner chicken dinner
            if (len(myDeck)==0):
                print 'GANHEIIII'
                sender.sendto('Ganhei',(TARGET,SD_port))
                in_game = False

            #throw card
            else:
                #Pass token
                sender.sendto(TOKEN,(TARGET,SD_port))
                has_token = False

        #pass turn
        else:
            if (lastCard.number == "+2"):
                deck_over = drawCards(deck,myDeck,2, stack)
                sender.sendto('compra2',(TARGET, SD_port))
                data, addr = receiver.recvfrom(50) #retira mensagem da rede
                if deck_over:
                    data = pickle.dumps(deck)
                    sender.sendto(data, (TARGET,SD_port))
                    data, addr = receiver.recvfrom(3580)

                print "Comprei a carta "+ colored("({},{})".format(myDeck[len(myDeck)-1].number, myDeck[len(myDeck)-1].color),myDeck[len(myDeck)-1].color)
                print "Comprei a carta "+ colored("({},{})".format(myDeck[len(myDeck)-2].number, myDeck[len(myDeck)-2].color),myDeck[len(myDeck)-2].color)

            if (lastCard.number == '@'):
                sender.sendto('Pulado',(TARGET, SD_port))
                data, addr = receiver.recvfrom(50) #retira mensagem da rede
            blocked = False
            #Pass token
            sender.sendto(TOKEN,(TARGET,SD_port))
            has_token = False

        player_turn = (player_turn +1) % N_PLAYERS



#recepcao
    data, addr = receiver.recvfrom(128) #buffer
    if (data == TOKEN):
        has_token = True
    else:
        sender.sendto(data,(TARGET,SD_port))
        if(data == 'Ganhei'):
            print'Jogador %s ganhou, a partida acabou'%((player_turn - 1) % N_PLAYERS)
            in_game = False
        elif (data == 'UNO!'):
            print'Jogador %s chamou UNO'%(player_turn)

        elif (data == "compra1"):
            print'Jogador %s comprou 1 carta'%(player_turn)
            if len(deck) == 0:
                data, addr = receiver.recvfrom(3580) #deck size max 2397
                sender.sendto(data,(TARGET,SD_port))
                deck = pickle.loads(data)
                stack = list()
                pass
            else:
                deck.pop()
            player_turn = (player_turn +1 ) % N_PLAYERS

        elif (data == 'compra2'):
            print('Jogador %s comprou 2 cartas'%(player_turn))
            if (len(deck) < 2):
                data, addr = receiver.recvfrom(3580) #deck size max 2397
                sender.sendto(data,(TARGET,SD_port))
                deck = pickle.loads(data)
                stack = list()
            else:
                deck.pop()
                deck.pop()
            player_turn = (player_turn +1 ) % N_PLAYERS
            
        elif (data == 'Pulado'):
            print 'Jogador %s foi pulado'%(player_turn)
            player_turn = (player_turn +1 ) % N_PLAYERS
        else:
            lastCard = pickle.loads(data)
            stack.append(lastCard);
            print "Jogador %s jogou"%(player_turn) + colored("({},{})".format(lastCard.number, lastCard.color),lastCard.color)

            #special cards
            if (lastCard.number == '+2'):
                if(player_turn == ((id - 1)% N_PLAYERS)):
                    blocked = True

            elif (lastCard.number == '@'):
                if(player_turn == ((id - 1)% N_PLAYERS)):
                    blocked = True

            player_turn = (player_turn +1 ) % N_PLAYERS
